import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Countries from './component/Countries';
import CountryDetail from './component/CountryDetail';
import { PageNotFound } from './component/PageNotFound';
import Region from './component/Region';

const AppRouter = () => (
    <Switch>
        <Route path="/" component={ Countries } exact={ true } />
        <Route path="/name" component={ PageNotFound } exact={ true } />
        <Route path="/name/:name" component={ CountryDetail } exact={ true } />
        <Route path="/:region" component={ Region } exact={ true } />
        <Route component={ PageNotFound } />
    </Switch>
);

export default AppRouter;
