import React from 'react';
import axios from 'axios';
import { NavLink } from 'react-router-dom';
import { Form, FormGroup, Input, Label, InputGroup, InputGroupButton, Card, CardBody, CardTitle } from 'reactstrap';
import '../style/Countries.css';

class Region extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      countries: []
    }
  }
  
  fetchData = (region) => {
    var url = region = 'https://restcountries.eu/rest/v2/region/' + region
    axios
    .get(url)
    .then(({ data }) => {
      this.setState({
        countries: data
      });
    })
    .catch((err) => { })
  }

  componentDidMount() {
    this.fetchData(this.props.match.params.region);
  }

  componentWillReceiveProps(nextProps){
      this.fetchData(nextProps.match.params.region);
  }

  onFormSubmit(e) {
    e.preventDefault();
    const option = e.target.elements.options.value;
    const search = e.target.elements.searchs.value;

    let url = 'https://restcountries.eu/rest/v2/'+ option + '/' + search
    axios
      .get(url)
      .then(({ data }) => {
        this.setState({
          countries: data
        });
      })
      .catch((err) => {})

    e.target.elements.searchs.value='';
  }

  render() {

    const CountryList = this.state.countries.map((el, index) => {
      return (
        <div key={index} className="col-sm-3 col-md-4 col-lg-2 mb-4 countries">
          <NavLink to={'name/'+el.name}>
            <Card  style={{ backgroundColor: 'rgba(0, 0, 0, 0.03)' }} >
              <CardTitle style={{ marginBottom: 0, marginTop: 10, fontSize: 16 }} className="text-center">{el.alpha2Code}</CardTitle>
              <CardBody style={{ padding: 10 }}>
                <img src={el.flag} alt="Flag" className="img-fluid"/>
              </CardBody>
            </Card>
          </NavLink>
        </div>
      );
    });

    return (
      <div className="row">
        <div className="col-sm-12">
          <Form inline onSubmit={this.onFormSubmit.bind(this)} className="my-3">
            <InputGroup className="mb-3">
              <Input type="text" name="searchs" placeholder="Search..." required autoComplete="off" />
              <InputGroupButton color="info">SEARCH</InputGroupButton>
            </InputGroup>
            <FormGroup className="mb-3">
              <Label className="mx-3">Search by: </Label>
              <Input type="select" name="options">
                <option value="name">Name</option>
                <option value="capital">City</option>
                <option value="callingcode">Calling Code</option>
              </Input>
            </FormGroup>
          </Form>
        </div>
        {CountryList}
      </div>
    );
    
  }
}

export default Region;
