import React from 'react';

export class PageNotFound extends React.Component{ 
    render() {
        return(
            <h1 style={{ textAlign: 'center', marginTop: '10%', color: '#333'}}>Error 404 Page Not Found</h1>
        )
    }
}
